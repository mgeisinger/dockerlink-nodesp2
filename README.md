### Description
Run broadlink-nodesp2 in a minimal python-alpine container. Broadlink-NodeSP2 is a node.js server with a python script to control Broadlink SmartPlug2 smart plugs.

## Build (optional)

    docker build -t dockerlink-nodesp2 .

## Run

    docker run -d -p 3000:3000 --name dockerlink-nodesp2 mgeisinger/dockerlink-nodesp2
    
The server is now listening on port 3000 for any requests. The basic URL syntax is

    http://server:3000?ip=[SP2 IP ADDRESS]&mac=[SP2 MAC ADDRESS]&state=[0,1,check]
    
##### SP2 IP ADDRESS
The ip address of the broadlink SP2 smart plug. Since docker doesn't know your DNS server it needs to be the ip address.
##### SP2 MAC ADDRESS
The mac address of the broadlink SP2 smart plug. Can be found on your router for example.
##### STATE
Here you can give three different states. '0' to turn off the plug, '1' to turn the plug on, and 'check' to get the current state of the plug.


### Example
    curl "http://localhost:3000?ip=`dig +short dnsnameOfYourPlug`&mac=123456abcdef&state=check"
Using `dig` with a `curl` call you can use the DNS name of the plug in your network. No need to remember ip addresses or change your scripts or configs when the address changes.

This example will return either
    `true`
or
    `false`
depending on your plug being turned on or off.

    curl "http://localhost:3000?ip=`dig +short dnsnameOfYourPlug`&mac=123456abcdef&state=1"
Will turn on the plug (if it is not on already)

And
    
    curl "http://localhost:3000?ip=`dig +short dnsnameOfYourPlug`&mac=123456abcdef&state=0"
Will turn it off (if it is on)

You can call the server by any means that can call a http server, for example use the url from above in your browser. You only need to replace the `dig` command with the ip address.

### Links
This container uses the python script 
https://bitbucket.org/mgeisinger/broadlink-nodesp2
to talk to the smart plug, which is a fork of 
https://github.com/NightRang3r/Broadlink-NodeSP2
. To use the check-state command `python-broadlink` library needs to be version 0.4 or higher.